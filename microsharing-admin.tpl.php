<div class="customize-wrapper">
	<div id="services">
		<h4><?php print t('Choose social services'); ?></h4>
    <div id="sharing-icon-wrap">
      <span id="fb-icon" data-value="facebook" class="refresh sharing-options active"><a href="#" title="facebook" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="tw-icon" data-value="twitter" class="refresh sharing-options active"><a href="#" title="twitter" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="em-icon" data-value="email" class="refresh sharing-options active"><a href="#" title="email" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="pn-icon" data-value="pinterest" class="refresh sharing-options active"><a href="#" title="pinterest" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="li-icon" data-value="linkedin" class="refresh sharing-options active"><a href="#" title="linkedin" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="ev-icon" data-value="evernote" class="refresh sharing-options active"><a href="#" title="evernote" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="gp-icon" data-value="googleplus" class="refresh sharing-options active"><a href="#" title="googleplus" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="vk-icon" data-value="vk" class="refresh sharing-options active"><a href="#" title="vk" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="od-icon" data-value="odnoklassniki" class="refresh sharing-options active"><a href="#" title="odnoklassniki" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="mm-icon" data-value="moimir" class="refresh sharing-options active"><a href="#" title="moimir" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="lj-icon" data-value="livejournal" class="refresh sharing-options active"><a href="#" title="livejournal" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="deli-icon" data-value="delicious" class="refresh sharing-options"><a href="#" title="delicious" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="glbookm-icon" data-value="google_bookmarks" class="refresh sharing-options"><a href="#" title="google_bookmarks" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="yahbookm-icon" data-value="yahoo_bookmarks" class="refresh sharing-options"><a href="#" title="yahoo_bookmarks" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="wapp-icon" data-value="whatsapp" class="refresh sharing-options"><a href="#" title="whatsapp" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="appn-icon" data-value="appnet" class="refresh sharing-options"><a href="#" title="appnet" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="xn-icon" data-value="xing" class="refresh sharing-options"><a href="#" title="xing" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="tvid-icon" data-value="twiddla" class="refresh sharing-options"><a href="#" title="twiddla" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="lifest-icon" data-value="lifestream" class="refresh sharing-options"><a href="#" title="lifestream" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="box-icon" data-value="box" class="refresh sharing-options"><a href="#" title="box" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="nelo-icon" data-value="netlog" class="refresh sharing-options"><a href="#" title="netlog" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="fun-icon" data-value="funp" class="refresh sharing-options"><a href="#" title="funp" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="xe-icon" data-value="xerpi" class="refresh sharing-options"><a href="#" title="xerpi" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="bib-icon" data-value="bibsonomy" class="refresh sharing-options"><a href="#" title="bibsonomy" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="bofr-icon" data-value="bookmarksfr" class="refresh sharing-options"><a href="#" title="bookmarksfr" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="ff-icon" data-value="friendfeed" class="refresh sharing-options"><a href="#" title="friendfeed" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="yigg-icon" data-value="yigg" class="refresh sharing-options"><a href="#" title="yigg" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="pus-icon" data-value="pusha" class="refresh sharing-options"><a href="#" title="pusha" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="fark-icon" data-value="fark" class="refresh sharing-options"><a href="#" title="fark" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="via-icon" data-value="viadeo" class="refresh sharing-options"><a href="#" title="viadeo" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="dz-icon" data-value="dzone" class="refresh sharing-options"><a href="#" title="dzone" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="fld-icon" data-value="folkd" class="refresh sharing-options"><a href="#" title="folkd" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="prfr-icon" data-value="printfriendly" class="refresh sharing-options"><a href="#" title="printfriendly" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="redi-icon" data-value="rediff" class="refresh sharing-options"><a href="#" title="rediff" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="stumbon-icon" data-value="stumbleupon" class="refresh sharing-options"><a href="#" title="stumbleupon" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="digg-icon" data-value="digg" class="refresh sharing-options"><a href="#" title="digg" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="wan-icon" data-value="wanelo" class="refresh sharing-options"><a href="#" title="wanelo" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="buf-icon" data-value="bufferapp" class="refresh sharing-options"><a href="#" title="bufferapp" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="mend-icon" data-value="mendeley" class="refresh sharing-options"><a href="#" title="mendeley" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="pinbin-icon" data-value="pinboard" class="refresh sharing-options"><a href="#" title="pinboard" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="flip-icon" data-value="flipboard" class="refresh sharing-options"><a href="#" title="flipboard" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="ymsgr-icon" data-value="yahoomessanger" class="refresh sharing-options"><a href="#" title="yahoomessanger" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="blogc-icon" data-value="blogger" class="refresh sharing-options"><a href="#" title="blogger" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="ffilter-icon" data-value="fivefilters" class="refresh sharing-options"><a href="#" title="fivefilters" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="citlike-icon" data-value="citeulike" class="refresh sharing-options"><a href="#" title="citeulike" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="insp-icon" data-value="instapaper" class="refresh sharing-options"><a href="#" title="instapaper" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="netv-icon" data-value="netvouz" class="refresh sharing-options"><a href="#" title="netvouz" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="blogm-icon" data-value="blogmarks" class="refresh sharing-options"><a href="#" title="blogmarks" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="menme-icon" data-value="meneame" class="refresh sharing-options"><a href="#" title="meneame" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="protop-icon" data-value="protopage" class="refresh sharing-options"><a href="#" title="protopage" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="webn-icon" data-value="webnews" class="refresh sharing-options"><a href="#" title="webnews" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="yomob-icon" data-value="youmob" class="refresh sharing-options"><a href="#" title="youmob" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="allvoi-icon" data-value="allvoices" class="refresh sharing-options"><a href="#" title="allvoices" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="care2-icon" data-value="care2" class="refresh sharing-options"><a href="#" title="care2" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="newstr-icon" data-value="newstrust" class="refresh sharing-options"><a href="#" title="newstrust" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="tue-icon" data-value="tuenti" class="refresh sharing-options"><a href="#" title="tuenti" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="redd-icon" data-value="reddit" class="refresh sharing-options"><a href="#" title="reddit" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="tum-icon" data-value="tumblr" class="refresh sharing-options"><a href="#" title="tumblr" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="mysp-icon" data-value="myspace" class="refresh sharing-options"><a href="#" title="myspace" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="wong-icon" data-value="wong" class="refresh sharing-options"><a href="#" title="wong" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="ork-icon" data-value="orkut" class="refresh sharing-options"><a href="#" title="orkut" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="getp-icon" data-value="getpocket" class="refresh sharing-options"><a href="#" title="getpocket" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="sprgpad-icon" data-value="springpadit" class="refresh sharing-options"><a href="#" title="springpadit" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="arto-icon" data-value="arto" class="refresh sharing-options"><a href="#" title="arto" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="plurk-icon" data-value="plurk" class="refresh sharing-options"><a href="#" title="plurk" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="typd-icon" data-value="typepad" class="refresh sharing-options"><a href="#" title="typepad" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="baidu-icon" data-value="baidu" class="refresh sharing-options"><a href="#" title="baidu" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="jumta-icon" data-value="jumptags" class="refresh sharing-options"><a href="#" title="jumptags" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="phefav-icon" data-value="phonefavs" class="refresh sharing-options"><a href="#" title="phonefavs" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="dii-icon" data-value="diigo" class="refresh sharing-options"><a href="#" title="diigo" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="strtid-icon" data-value="startaid" class="refresh sharing-options"><a href="#" title="startaid" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="yolnk-icon" data-value="yoolink" class="refresh sharing-options"><a href="#" title="yoolink" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="nevi-icon" data-value="newsvine" class="refresh sharing-options"><a href="#" title="newsvine" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="blklst-icon" data-value="blinklist" class="refresh sharing-options"><a href="#" title="blinklist" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="sldot-icon" data-value="slashdot" class="refresh sharing-options"><a href="#" title="slashdot" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="jaspt-icon" data-value="jamespot" class="refresh sharing-options"><a href="#" title="jamespot" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="budmar-icon" data-value="buddymarks" class="refresh sharing-options"><a href="#" title="buddymarks" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="symloo-icon" data-value="symbaloo" class="refresh sharing-options"><a href="#" title="symbaloo" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="zakl-icon" data-value="zakladok" class="refresh sharing-options"><a href="#" title="zakladok" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="lkme-icon" data-value="linkme" class="refresh sharing-options"><a href="#" title="linkme" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="adifni-icon" data-value="adifni" class="refresh sharing-options"><a href="#" title="adifni" ><div class="gtc-icon-sprite"></div></a></span>
      <span id="amenme-icon" data-value="amenme" class="refresh sharing-options"><a href="#" title="amenme" ><div class="gtc-icon-sprite"></div></a></span>
		</div>
	</div>
		    			
	<div id="icon-color" >
		<h4><?php print t('Color icon services'); ?></h4>
		<div id="icon-colorSelector"></div>
	</div>
	
	<div id="serviceBackground-color" >
		<h4><?php print t('BG icon services'); ?></h4>
		<div id="serviceBackground-colorSelector"></div>
	</div>
	
	<div id="highlight-color" >
		<h4><?php print t('Adjust background for display text of quote'); ?></h4>
		<div id="highlight-colorSelector"></div>
	</div>

	<div id="text-color" >
		<h4><?php print t('Adjust color text of quote'); ?></h4>
		<div id="text-colorSelector"></div>
	</div>
	
  <div id="text-sharing-customize-panel" >
		<h4><?php print t('Set opportunity to sharing quote of text'); ?></h4>
		<div class="switch small round">
			<div class="first-radio-off">
  			<label for="text-sharing-off" ><?php print t('No'); ?></label>
  		  <input id="text-sharing-off" name="switch-text-sharing" type="radio">
		  </div>
		  <div>
  		  <label for="text-sharing-on"><?php print t('Yes'); ?></label>
  		  <input id="text-sharing-on" name="switch-text-sharing" type="radio" checked>
		  </div>
		</div>
	</div>

	<div id="img-sharing-customize-panel" >
		<h4><?php print t('Set opportunity to sharing images and video when mouse hover on a object'); ?></h4>
		<div class="switch small round">
    	<div class="first-radio-off">
    		<label for="img-sharing-off"><?php print t('No'); ?></label>
    	  <input id="img-sharing-off" name="switch-img-sharing" type="radio">
		  </div>
		  <div>
  			<label for="img-sharing-on"><?php print t('Yes'); ?></label>
  		  <input id="img-sharing-on" name="switch-img-sharing" type="radio" checked>
  		</div>
		</div>
	</div>

  <div id="page-sharing-customize-panel" >
    <h4><?php print t('Set opportunity sharing a link on page'); ?></h4>
    <div class="switch small round">
      <div class="first-radio-off">
       	<label for="page-sharing-off"> <?php print t('No'); ?> </label>
    	  <input id="page-sharing-off" name="switch-page-sharing" type="radio">
  	  </div>
		  <div>
    	  <label for="page-sharing-on"><?php print t('Yes'); ?></label>
        <input id="page-sharing-on" name="switch-page-sharing" type="radio" checked>  
      </div>
  	</div>
  </div>
</div>
<div id="example-preview">
	<img src="/<?php print drupal_get_path('module', 'microsharing'); ?>/images/preview-example.jpeg" class="preview-example-img"> 
	<h6><?php print t('Highlight part of text below'); ?></h6>
	<div id="example-preview-text-sharing">
	<pre class="example-review-text">
		<p>
<?php print t('How to attract traffic, users of social networking, hits on your site, blog or forum? Very simple - you need to write a beautiful post to your group or feed!
Actually better to everyone who comes to your site will be able write a beautiful post in their groups and feed. How is that possible? 

Very simple. We offer for you micro sharing. Using browser extensions and widget on the site, users can send a piece of text, images or even video in social networks. The more users will do it, the more users will see the post in social networks, the more people will come to your site, blog or forum.
Many social networks offer to put their social button. But it is not as effective as micro sharinga with service 4qu.co
When the user clicks "Like" sent the same titles, the same image and the same description.
From widget micro sharing all messages in social networks are individual, because every share only that it seemed really important.
Thus the number of posts with links to your website in social networks is growing exponentially, and you get free traffic!
Put a widget for micro sharing and increase the profits of your project!'); ?>
		</p>
  </pre>
  </div>
</div>
<div class="form-setting-code">
<?php print $render_form;?>
</div>